const cities = document.querySelectorAll('.cities')

const select = document.querySelector('.select')

select.addEventListener('change', func)

function func() {
cities.forEach( elem => {
    if(elem.value == select.value)
    fetch(`http://api.openweathermap.org/data/2.5/weather?q=${elem.value}&appid=2933f18b5f3f841ded387f41cb595dea`)
    .then(function(resp) { return resp.json()})
    .then(function(data){
        // console.log(data)
        document.querySelector('.city').innerHTML = elem.value
        document.querySelector('.temp').innerHTML = Math.round(data.main.temp - 273) + '&deg;';
        document.querySelector('.description').textContent = data.weather[0]['description'];
        document.querySelector('.icon').innerHTML = `<img src="https://openweathermap.org/img/wn/${data.weather[0]['icon']}@2x.png">`;
    })
})
}
