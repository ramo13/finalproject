import * as elem from "./CreateElements.js";

export const CreateMainPage = () => {
    const maindiv = elem.createDiv('main');
    const h1 = document.createElement('h1');
    const p = document.createElement('p');
    h1.textContent = 'Weather';
    maindiv.append(h1);
    return maindiv
}