import * as elem from './CreateElements.js';

export const CreateMainButtons = () => {
    const divForButton = elem.createDiv('container')
    const loginButton = elem.createButton('Войти', 'login__button')
    divForButton.append(loginButton, CreateRegButton())
    return divForButton
}

export const CreateRegButton = () => {
    const RegButton = elem.createButton('Зарегистрироваться', 'login__button')
    return RegButton
}