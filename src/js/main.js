import { CreateRegistrationForm, CreateLoginForm } from "./Registration.js";
import { CreateMainButtons} from "./MainButtons.js";
import {CreateMainPage} from './MainPage.js'

let root = document.querySelector(".root");

root.append(CreateMainButtons(), CreateLoginForm(), CreateRegistrationForm());
root.prepend(CreateMainPage());

const Forms = Array.from(document.querySelectorAll(".form"));
const [LoginForm, RegForm] = Forms;
const MainButtons = Array.from(document.querySelectorAll(".login__button"));
const [RegButton, LoginButton] = MainButtons;
const FormsHidden = Array.from(document.querySelectorAll(".form__hidden"));
const [RegFormHidden, LoginFormHidden] = FormsHidden;
const ButtonsClose = Array.from(document.querySelectorAll(".button__close"));
const [ButtonRegClose, ButtonLoginClose] = ButtonsClose;
const textSuccess = Array.from(document.querySelectorAll('h2'))
const [, regText] = textSuccess
const check = document.querySelector('.check')
const username = document.querySelector('.name')
const email = document.querySelector('.email')
const pass = document.querySelector('.pass')
const pass2 = document.querySelector('.pass2')
const emailLogin = document.querySelector('.email2')
const passwordLogin = document.querySelector('.pass3')

class User {
  constructor(options) {
          this.name = username.value;
          this.password = pass.value;
          this.email = email.value;
          this.pass2 = pass2.value;
      }
}

function reg() {
  let emailValue = email.value;
  let passValue = pass.value;
  let pass22Value = pass2.value;
  let nameValue = username.value;
  console.log(username)

  class people extends User {

  };
  let user = new people;
  console.log(user);
  if (nameValue === "") {
          setErrorFor(username, "Введите имя пользователя");
        } else {
          setSuccessFor(username);
        }
        if (emailValue === "") {
          setErrorFor(email, "Введите адрес почты");
        } else if (!Email(emailValue)) {
          setErrorFor(email, "Адрес введен некорректно");
        } else if (localStorage.getItem(email.value)) {
          setErrorFor(email, 'Такой пользователь уже существует')
        } else setSuccessFor(email);
        if (passValue === "") {
          setErrorFor(pass, "Пароль не введен");
        } else {
          setSuccessFor(pass);
        }
        if (pass22Value === "") {
          setErrorFor(pass2, "Вы не ввели пароль");
        } else if (passValue !== pass22Value) {
          setErrorFor(pass, 'Пароли не совпадают')
          setErrorFor(pass2, "Пароли не совпадают");
        } else {
          setSuccessFor(pass2);

          if(nameValue && emailValue && (emailValue !== localStorage.getItem(email.value)) && passValue && passValue == pass22Value ) {
             localStorage.setItem(email.value, JSON.stringify(user));
             regText.innerHTML = 'Регистрация прошла успешно'
             LoginFormHidden.classList.toggle("show")
          }
};
}

let currentUser;
function logIn() {
  let email2 = document.querySelector('.email2');
  let pass2 = document.querySelector('.pass3');
  currentUser = JSON.parse(localStorage.getItem(email2.value));
  console.log(pass2.type)

    if (currentUser == null) {
      setErrorFor(email2, 'Введите имя пользователя')
    } else if (pass2.value !== currentUser.password) {
      setErrorFor(pass2, 'Пароли не совпадают');
    } else if(pass2.value === '') {
      setErrorFor(pass2, 'Вы не ввели пароль')
    }
     else {
        window.location.href = "weather.html";
    };
    localStorage.setItem("currentUser", JSON.stringify(currentUser));
    return currentUser;
}

  const setErrorFor = (input, message) => {
    const inputForm = input.parentElement;
    const textform = inputForm.querySelector("p");
    inputForm.className = "form__element error";
    textform.innerText = message;
  }
  
  const setSuccessFor = (input) => {
    const inputForm = input.parentElement;
    inputForm.className = "form__element success";
  }
  
  const Email = (email) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  
  check.addEventListener("change", function (e) {
    const target = e.target;
    if (target.checked) {
      document.querySelector('.pass3').type = 'text'
    } else {
      document.querySelector('.pass3').type = 'password'
    }
  });

  LoginForm.addEventListener("submit", (event) => {
    event.preventDefault()
    logIn();
  });
  
  RegForm.addEventListener("submit", (event) => {
    event.preventDefault()
    reg()
  });
  
  LoginButton.addEventListener("click", () => {
    LoginFormHidden.classList.toggle("show");
  });
  
  RegButton.addEventListener("click", () => {
    RegFormHidden.classList.toggle("show");
  });
  
  ButtonRegClose.addEventListener("click", () => {
    RegFormHidden.classList.toggle("show");
    emailLogin.value = '';
    passwordLogin.value = '';
    const a = Array.from(LoginForm.getElementsByClassName("form__element"));
    a.forEach((elem) => (elem.className = "form__element"));
  });
  
  ButtonLoginClose.addEventListener("click", () => {
    LoginFormHidden.classList.toggle("show");
    document.querySelector('.email').value = '';
    document.querySelector('.name').value = '';
    document.querySelector('.pass').value = '';
    document.querySelector('.pass2').value = '';

    const b = Array.from(RegForm.getElementsByClassName("form__element"));
    b.forEach((elem) => (elem.className = "form__element"));
  });
  
  RegFormHidden.addEventListener("click", function (e) {
    if (!e.target.closest(".form__body")) {
      RegFormHidden.classList.toggle("show");
    }
  });
  
  LoginFormHidden.addEventListener("click", function (e) {
    if (!e.target.closest(".form__body")) {
      LoginFormHidden.classList.toggle("show");
    }
  });
  