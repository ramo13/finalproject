import * as elem from "./CreateElements.js";

const labelUsername = elem.createLabel('label__content', 'Имя пользователя')
const labelEmail = elem.createLabel('label__content', 'Почтовый адрес')
const labelPassword = elem.createLabel('label__content', 'Пароль')
const labelPasswordRepeat = elem.createLabel('label__content', 'Повторите пароль')
const labelEmail2 = elem.createLabel('label__content_login', 'Почтовый адрес')
const labelPassword2 = elem.createLabel('label__content_login', 'Пароль')
const inputUsername = elem.createInput('Введите ваше имя', 'text', 'name')
const inputEmail = elem.createInput('', 'email', 'email')
const inputPassword = elem.createInput('', 'password', 'pass')
const inputPasswordRepeat = elem.createInput('', 'password', 'pass2')
const p1 = document.createElement('p')
const p2 = document.createElement('p')
const p3 = document.createElement('p')
const p4 = document.createElement('p')
const p5 = document.createElement('p')
const p6 = document.createElement('p')


export const CreateRegistrationForm = () => {
  //Переменные - элементы
  const form = elem.createDiv("form__hidden");
  const form__body = elem.createDiv("form__body");
  const formmain = document.createElement('form');
  const formElement1 = elem.createDiv('form__element')
  const formElement2 = elem.createDiv('form__element')
  const formElement3 = elem.createDiv('form__element')
  const formElement4 = elem.createDiv('form__element')
  const form__button = elem.createInput("", "submit", "reg__button");
  const button__close = elem.createButton("X", "button__close");
  const buttonReset = elem.createButton("Очистить форму", "form_reset");
  const h2 = document.createElement("h2");
  formmain.classList.add('form');
  //Контент
  form__button.value = "Зарегистрироваться";
  buttonReset.setAttribute("type", "reset");
  h2.textContent = "Зарегистрируйтесь";
  //Добавление элементов
  formElement1.append(labelUsername, inputUsername,p1)
  formElement2.append(labelEmail, inputEmail, p2)
  formElement3.append(labelPassword, inputPassword, p3)
  formElement4.append(labelPasswordRepeat, inputPasswordRepeat, p4)
  formmain.append(formElement1,formElement2,formElement3,formElement4,buttonReset,form__button)
  formmain.prepend(h2);
  form__body.append(formmain, button__close);
  form.append(form__body);
  return form;

}

export const CreateLoginForm = () => {
const inputEmail2 = elem.createInput('', 'email', 'email2')
const inputPassword2 = elem.createInput('', 'password', 'pass3')
  //Переменные - элементы
  const form = elem.createDiv("form__hidden");
  const form__body = elem.createDiv("form__body");
  const formmain = document.createElement('form');
  const formElement1 = elem.createDiv('form__element')
  const formElement2 = elem.createDiv('form__element')
  const formElement3 = elem.createDiv('form__element')
  const form__button = elem.createButton("Войти", "sign__button");
   const button__close = elem.createButton("X", "button__close");
   const buttonReset = elem.createButton("Очистить форму", "form_reset");
   const h2 = document.createElement("h2");
   const check = elem.createInput('','checkbox', 'check')
   const checkLabel = elem.createLabel('checkLabel', 'Показать пароль')
   //Контент 
   buttonReset.setAttribute("type", "reset");
  formmain.classList.add('form');
  button__close.setAttribute("type", "reset");
  h2.textContent = "Войти в учетную запись";
  //Добавление элементов
  formElement1.append(labelEmail2, inputEmail2,p5)
  formElement2.append(labelPassword2, inputPassword2,p6)
  formElement3.append(checkLabel, check)
  formmain.append(formElement1,formElement2,formElement3,buttonReset,form__button)
  formmain.prepend(h2)
  form__body.append(formmain, button__close);
  form.append(form__body);
  return form;

}

