export const createLabel = (nameClass, text) => {
    const label = document.createElement('label');
    label.classList.add(nameClass);
    label.textContent = text;
    return label
}

export const createDiv = (name) => {
    const div = document.createElement('div')
    div.classList.add(name);
    return div
}

export const createButton = (title, name) => {
    const button = document.createElement('button');
    button.classList.add(name);
    button.textContent = title;
    return button
}

export const createInput = (nameHolder, nameType, nameClass) => {
    const input = document.createElement('input');
    input.setAttribute('placeholder', nameHolder);
    input.setAttribute('type', nameType);
    input.classList.add(nameClass);
    return input
}
